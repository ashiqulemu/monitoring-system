<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="description" content=" ">

    <title> Attendents Monitoring System  </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    {{--<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.0/css/all.css' >--}}
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    <link rel="icon" href="{{ asset('images/favi.png') }}" type="image/gif" sizes="16x16">
</head>

<body class="app sidebar-mini rtl">
<div id="app">
    <flash-message class="myCustomClass"></flash-message>
    @yield('content')


</div>
<script src="{{ asset('/js/app.js') }}"></script>
<script src="{{ asset('/js/theme.js') }}"></script>
<script src="{{ asset('/js/pace.min.js') }}"></script>

</body>
</html>
