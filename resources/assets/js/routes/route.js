module.exports = [
    {
        path: '/dashboard',
        name: 'dashboard',
        component: require('../views/dashboard.vue')
    },
    {
        path: '/add',
        name: 'add',
        component: require('../views/pages/add.vue')
    },
    {
        path: '/',
        name: 'userLogin',
        component: require('../views/users/login.vue')
    },
    {
        path: '/time-events',
        name: 'timeEvents',
        component: require('../views/users/time-events.vue')
    },
    {
        path: '/employee/create',
        name: 'employeeCreate',
        component: require('../views/employee/create.vue')
    },
    {
        path: '/employee',
        name: 'employeeList',
        component: require('../views/employee/list.vue')
    },
    {
        path: '/employee/:id/edit',
        name: 'employeeEdit',
        component: require('../views/employee/edit.vue')
    },

    {
        path: '/attendance/create',
        name: 'attendanceCreate',
        component: require('../views/attendance/create.vue')
    },
    {
        path: '/attendance',
        name: 'attendanceList',
        component: require('../views/attendance/list.vue')
    },
    {
        path: '/attendance/:id/edit',
        name: 'attendanceEdit',
        component: require('../views/attendance/edit.vue')
    },

    {
        path: '/pre-rota/create',
        name: 'prerotaCreate',
        component: require('../views/pre-rota/create.vue')
    },
    {
        path: '/pre-rota',
        name: 'prerotaList',
        component: require('../views/pre-rota/list.vue')
    },
    {
        path: '/pre-rota/:id/edit',
        name: 'prerotaEdit',
        component: require('../views/pre-rota/edit.vue')
    },
    {
        path: '/product/create',
        name: 'productCreate',
        component: require('../views/product/create.vue')
    },
    {
        path: '/product',
        name: 'productList',
        component: require('../views/product/list.vue')
    },
    {
        path: '/product/:id',
        name: 'productEdit',
        component: require('../views/product/edit.vue')
    },
    {
        path: '/stock/create',
        name: 'stockCreate',
        component: require('../views/stock/create.vue')
    },
    {
        path: '/stock',
        name: 'stockList',
        component: require('../views/stock/list.vue')
    },
    {
        path: '/stock/:id',
        name: 'stockEdit',
        component: require('../views/stock/edit.vue')
    },
    {
        path: '/report/employee',
        name: 'employeeReport',
        component: require('../views/report/employeeReport.vue')
    },
    {
        path: '/report/holiday',
        name: 'holidayReport',
        component: require('../views/report/holiday-report.vue')
    },
    {
        path: '/report/stock',
        name: 'stockReport',
        component: require('../views/report/stockReport.vue')
    },
    {
        path: '/report/stock-staff',
        name: 'stockReportUser',
        component: require('../views/report/stockReportUser.vue')
    },
    {
        path: '/report/pay-slip',
        name: 'paySlip',
        component: require('../views/report/pay-slip.vue')
    },
    {
        path: '/holiday',
        name: 'holiday',
        component: require('../views/holiday/create.vue')
    },
    {
        path: '/user',
        name: 'userDetails',
        component: require('../views/users/userDeatils.vue')
    },

    {
        path: '/user/create',
        name: 'userCreate',
        component: require('../views/users/userCreate.vue')
    },
    {
        path: '/user/:id/edit',
        name: 'userEdit',
        component: require('../views/users/userEdit.vue')
    },

    { path: "*", component: {
        template: '<h3 class="text-center">Oh No! Page not Found</h3>'
    }
    },





]
