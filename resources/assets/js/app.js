require('./bootstrap');
window.Vue = require('vue');



import Vue from 'vue'
import VueRouter from 'vue-router'
import {default as routeData} from './routes/route'
import moment from 'moment';
import VueFlashMessage from 'vue-flash-message';
import VeeValidate from 'vee-validate';
import vSelect from 'vue-select';

import DatePicker from 'vue2-datepicker'
Vue.use(VueFlashMessage);

Vue.prototype.moment = moment

Vue.use(VueRouter);
Vue.use(VeeValidate)

let routeList=[]
const routes=routeList.concat(
    routeData
)

Vue.filter('removeUs',function (val) {
    if(val){
        return val.replace('_',' ')
    }
})
const router= new VueRouter({
    routes,
    mode: 'history',
    linkActiveClass: 'open active',
    scrollBehavior: () => ({ y: 0 }),
})

Vue.component('admin-home', require('./views/home.vue'));
Vue.component('v-select', vSelect)
Vue.component('date-picker', DatePicker)
Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('header-component', require('./components/global/header.vue'));
Vue.component('sidebar-component', require('./components/global/sidebar.vue'));
Vue.component('footer-component', require('./components/global/footer.vue'));
Vue.component('data-table', require('./components/global/DataTable.vue'));
Vue.component('data-table-report', require('./components/global/dataTableReport.vue'));


new Vue({
    el: '#app',
    router,
    data(){
        return{
            auth:null

        }

    },
    created(){
        axios.get('/getauth').then(res=>{
            this.auth=res.data
        })
    },
    methods:{
        getTime(val){
            let time=val.slice(11,19)
            return time
        },
        differenceTwoTime(from,to){
            if(from && to){
                let fromTime = from.slice(11,19)
                    fromTime = moment(fromTime,"HH:mm:ss");
                let toTime = to.slice(11,19)
                    toTime = moment(toTime, "HH:mm:ss");
                  return toTime.diff(moment(fromTime), 'hours')+' h : '+toTime.diff(moment(fromTime), 'minutes')%60+' m : '+ toTime.diff(moment(fromTime), 'seconds')%60+' s';


            }

        },
        calculatAllTotalTime(val){
            let totalTime=0
            let newVal=JSON.parse('['+val+']')
            newVal.forEach(e=>{
                if(e.clock_in && e.clock_out){
                    let differentTime = this.differenceTwoTime(e.clock_in, e.clock_out)
                   let tTime=moment(differentTime, 'h [hours] mm [mins] ss [seconds]')

                    var diff = tTime.diff(tTime.clone().startOf('day'));
                    if(differentTime.includes("-")){
                        totalTime -= diff;
                    }else{
                        totalTime += diff;
                    }


                }
            })

            var dur = moment.duration(totalTime);
            return dur.hours()+' h : '+dur.minutes()+' m : '+dur.seconds()+' s'
        },
        needToParse(val){
            if(val){
                let newVal=JSON.parse('['+val+']')
                return newVal
            }

        },
        printMe(){
          window.print()
        },
        calAllTimeFromRaw(val){
            let totalTime=0
            val.forEach(e=>{
                if(e.clock_in && e.clock_out){
                    let differentTime = this.differenceTwoTime(e.clock_in, e.clock_out)
                    let tTime=moment(differentTime, 'h [hours] mm [mins] ss [seconds]')

                    var diff = tTime.diff(tTime.clone().startOf('day'));
                    if(differentTime.includes("-")){
                        totalTime -= diff;
                    }else{
                        totalTime += diff;
                    }


                }
            })

            var dur = moment.duration(totalTime);
            return  dur.hours()+dur.days()*24+' h : '+dur.minutes()+' m : '+dur.seconds()+' s'
        },
        pData(val){
            return parseFloat(val)?parseFloat(val):0

        },


    }
});

