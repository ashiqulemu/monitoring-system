<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'=>'Admin',
            'email'=>'admin@admin.com',
            'password'=>'$2y$12$98fNALmtrq4jue8Q9PMHDO2S/LcFjhJkwWSXoMHkKnU.4k62Hdch.',
            'role'=>'admin',
        ]);

        for($i=0; $i<10; $i++){

            $user=\App\User::create([
                'name'=>'user'.$i,
                'email'=>'user'.$i.'@diamondsgroup.com',
                'password'=>'$2y$12$98fNALmtrq4jue8Q9PMHDO2S/LcFjhJkwWSXoMHkKnU.4k62Hdch.',
                'role'=>'staff',
            ]);
            \App\Employee::create([
               'name'=>'user'.$i,
              'username'=>'user'.$i,
              'user_id'=>$user->id,
               'password'=>'$2y$12$k8hgU28TL0YflmkJBAbXBedr4LeAp4wN0lBMvIP3B6.9WK8WSTdQi',
                'employee_type'=>'full time',
             ]);
           }
    }
}
