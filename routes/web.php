<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::post('/staff/login', 'AttendanceController@staffLogin');
Route::post('/staff/logout', 'AttendanceController@staffLogout');

Route::group(['middleware'=>['checkAuth','role','jsonTrack']],function(){

    Route::resource('/attendance', 'AttendanceController');
    Route::resource('/employee', 'EmployeeController');
    Route::resource('/pre-rota', 'PreRotaController');
    Route::resource('/holiday', 'HolidayController');
    Route::get('/get-today-attendance', 'AttendanceController@getTodayAttendance');
    Route::get('/prerota/details', 'PreRotaController@getData');
    Route::get('/get-employee-list', 'EmployeeController@getEmployeeList');
    Route::get('/get-report/employee', 'ReportController@getEmployeeReport');
    Route::get('/get-report/holiday', 'HolidayController@getHolidayReport');
    Route::get('/get-dashboard-data', 'ReportController@getDashboardData');
    Route::get('/generate-pay-slip', 'ReportController@generatePayslip');
    Route::resource('/user', 'UserController');
    Route::get('/get-employee/holiday-count/{id}', 'EmployeeController@getHolidayCount');

});
Route::group(['middleware'=>['checkAuth','jsonTrack']],function(){
    Route::get('/product/get-all-list', 'ProductController@getAllProduct');
    Route::resource('/product', 'ProductController');
    Route::resource('/stock-maintenance', 'StockmaintainceController');
    Route::get('/get-report/stock', 'ReportController@getStockReport');
    Route::get('/get-report/stock-staff', 'ReportController@getStockReportByStaff');
    Route::get('/getauth', function (){
        return auth()->user();
    });
});

Route::get('/{all}', function () {
    if(auth()->user() || request()->path()=='/'){
        return view('home');
    }else{
        return redirect('/login');
    }

})->where(['all'=>'.*']);



