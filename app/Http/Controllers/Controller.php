<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function getListForUI($query, $request)
    {

        $data = null;
        if($request->input('searchValue')){

            if(strpos($request->input('searchKey'), '-') !== false){
                $query = $query->where(str_replace('-','.',
                    $request->input('searchKey')), 'LIKE','%'.$request->input('searchValue').'%');
            }
            elseif (strpos($request->input('searchKey'), '.') !== false) {
                $searchValue=$request->input('searchValue');
                $explodeSarchKey=explode('.',$request->input('searchKey'));
                $query->whereHas($explodeSarchKey[0], function($q) use ($searchValue,$explodeSarchKey){
                    $q->where($explodeSarchKey[1], 'LIKE', "%$searchValue%");
                });
            }else{
                $query = $query->where($request->input('searchKey'), 'LIKE','%'.$request->input('searchValue').'%');
            }

        }
        if($request->input('fromDate') && $request->input('toDate')){
            $query=$query->whereBetween('sm.date',[$request->input('fromDate'), $request->input('toDate')]);
        }

        if ($request->input('paging')) {
            if ($request->input('resultPerPage')=='All'){
                $data = $query->orderBy($request->input('sortName'),
                    $request->input('sortOrder'))->paginate($query->count());
            }else{
                $data = $query->orderBy($request->input('sortName'),
                    $request->input('sortOrder'))->paginate($request->input('resultPerPage'));
            }

        } else {
            $data = $query->orderBy($request->input('sortName'),
                $request->input('sortOrder'))->get();
        }
        return response()->json($data);
    }

}
