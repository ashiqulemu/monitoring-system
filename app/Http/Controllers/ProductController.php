<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(Request $request)
    {
        $query=Product::select();
        return $this->getListForUI($query, $request);
    }

    public function create()
    {
        //
    }


    public function store(ProductRequest $request)
    {
        Product::create($request->all());
        return response()->json([
            'type'=>'success',
            'message'=>'Product create successfully'
        ]);
    }

    public function show(Product $product)
    {
        //
    }


    public function edit(Request $request,$id)
    {
        return Product::find($id);
    }


    public function update(Request $request,$id)
    {
        Product::find($id)->update($request->all());
        return response()->json([
            'type'=>'success',
            'message'=>'Product updated successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return response()->json([
            'type'=>'success',
            'message'=>'Product deleted successfully'
        ]);
    }

    public function getAllProduct(){
        return Product::all();
    }
}
