<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use App\Holiday;
use App\Http\Requests\EmployeeRequest;
use App\PreRota;
use App\Stockmaintaince;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{

    public function index(Request $request)
    {
        $query=Employee::select();
        return $this->getListForUI($query, $request);
    }


    public function create()
    {
        //
    }


    public function store(EmployeeRequest $request)
    {
        $request->merge(['password'=>Hash::make($request->password)]);
        $user = User::create([
            'name'=>$request->name,
            'email'=>$request->username.'@diamondsgroup.com',
            'password'=>$request->password,
            'role'=>'staff',
        ]);
        $request->merge(['user_id'=>$user->id]);
        Employee::create($request->all());
        return response()->json([
           'type'=>'success',
           'message'=>'Staff create successfully'
        ]);
    }


    public function show(Employee $employee)
    {
        //
    }


    public function edit(Request $request, $id)
    {
        return Employee::find($id);
    }

    public function update(EmployeeRequest $request, $id)
    {

        $employee=Employee::find($id);
        if(!($employee->password == $request->password)){
            $request->merge(['password'=>Hash::make($request->password)]);
        }

        $user = User::whereId($employee->user_id)->first()->update([
            'name'=>$request->name,
            'email'=>$request->username.'@diamondsgroup.com',
            'password'=>$request->password,
        ]);
        $employee->update($request->all());
        return response()->json([
            'type'=>'success',
            'message'=>'Staff create successfully'
        ]);
    }


    public function destroy(Request $request, $id)
    {
        $attendace=Attendance::whereEmployeeId($id)->count();
        $holiday=Holiday::whereEmployeeId($id)->count();
        $stock=Stockmaintaince::whereEmployeeId($id)->count();
        $preRota=DB::table('employee_pre_rota')
            ->where('employee_id',$id)->count();
        if($attendace+$preRota+$holiday+$stock>0){
            return response()->json([
                'type'=>'error',
                'message'=>'Staff exist with Prorota, Attendance, Holiday, Stock Maintain!'
            ]);
        }else{

            $employee=Employee::find($id);
            $user=User::whereEmail($employee->username.'@diamondsgroup.com')->first();
            $user->delete();
            $employee->delete();
            return response()->json([
                'type'=>'success',
                'message'=>'Staff Deleted successfully'
            ]);
        }
    }

    public function getEmployeeList(){
        return Employee::whereIsActive(1)->get();
    }

    public function getHolidayCount($id){
        $contractDateBegin = date('Y') . '-04-01';
        $contractDateEnd = date('Y', strtotime('+1 year')) . '-03-31';

        $holidayDataCount=Holiday::whereEmployeeId($id)
            ->whereBetween('date',[$contractDateBegin,$contractDateEnd])
            ->where('type','Authorized holiday')
            ->count();

        return response()->json([
            'holidayDataCount'=>$holidayDataCount,

        ]);
    }
}
