<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use App\Holiday;
use App\Http\Requests\HolidayRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HolidayController extends Controller
{

    public function index(Request $request)
    {
        $query = Holiday::with('employee');
        return $this->getListForUI($query, $request);
    }


    public function create()
    {
        //
    }


    public function store(HolidayRequest $request,$updateMessage=null,$id=null)
    {
        $employeeId = $request->input('employee')['id'];

        $request->merge(['employee_id' => $employeeId]);
        $employee = Employee::whereId($employeeId)->first();
        $entitledHoliday = $employee->entitled_holiday;

        if ($entitledHoliday > 0 || ($request->input('type') == 'Unauthorized holiday')) {
            $hasHoliday = Holiday::whereEmployeeId($employeeId)
                ->whereDate('date', $request->input('date'))->count();
            if($id){
               $findById=Holiday::find($id);
               if($findById->date==$request->input('date') && $hasHoliday==1){
                   $hasHoliday=0;
               }
            }

            if (!$hasHoliday) {
                Holiday::create($request->except('employee'));
                if ($request->input('type') == 'Authorized holiday') {
                    $employee->update(['entitled_holiday' => ($entitledHoliday - 1)]);
                }

                if($id){
                    $this->destroy($id);
                }

                return response()->json([
                    'type' => 'success',
                    'message' => $updateMessage?$updateMessage:'Holiday Created successfully'
                ]);
            } else {
                return response()->json([
                    'type' => 'error',
                    'message' => 'This staff has Holiday Already exist in this date'
                ]);
            }

        } else {
            return response()->json([
                'type' => 'error',
                'message' => "This staff Authorized entitled holidays are over"
            ]);
        }

    }


    public function show(Holiday $holiday)
    {
        //
    }


    public function edit(Holiday $holiday)
    {
        //
    }


    public function update(HolidayRequest $request, $id)
    {

       return $this->store($request,'Holiday Updated successfully',$id);

    }


    public function destroy($id)
    {
        $holiday = Holiday::find($id);
        $employee = Employee::whereId($holiday->employee_id)->first();

        $contractDateBegin = date('Y') . '-04-01';
        $contractDateEnd = date('Y', strtotime('+1 year')) . '-03-31';
        if (($holiday->date > $contractDateBegin) && ($holiday->date < $contractDateEnd)) {

            if ($holiday->type == 'Authorized holiday') {
                $employee->update(['entitled_holiday' => ($employee->entitled_holiday + 1)]);
            }
        }
        $holiday->delete();
        return response()->json([
            'type' => 'success',
            'message' => 'Holiday Deleted successfully'
        ]);
    }


    public function getHolidayReport(Request $request)
    {
        $employeeId=request()->input('employee_id');
        $contractDateBegin = date('Y') . '-04-01';
        $contractDateEnd = date('Y', strtotime('+1 year')) . '-03-31';

        $employee= Employee::find($employeeId);
        $holidayData=Holiday::with('employee')
            ->whereEmployeeId($employeeId)
            ->whereBetween('date',[$contractDateBegin,Carbon::today()])
            ->where('type','Authorized holiday')
            ->get();
        $holidayDataCount=Holiday::whereEmployeeId($employeeId)
            ->whereBetween('date',[$contractDateBegin,$contractDateEnd])
            ->where('type','Authorized holiday')
            ->count();
        $sickDays = Attendance::whereEmployeeId($employeeId)
            ->whereBetween('clock_in',[$contractDateBegin,Carbon::today()])
            ->where('type','Sick')
            ->count();
        $absenceDays=Attendance::whereEmployeeId($employeeId)
            ->whereBetween('clock_in',[$contractDateBegin,Carbon::today()])
            ->where('type','Absence')
            ->count();

        return response()->json([
           'employee'=>$employee,
           'holidayData'=>$holidayData,
            'sickDays'=>$sickDays,
            'absenceDays'=>$absenceDays,
            'holidayDataCount'=>$holidayDataCount
        ]);
    }



}
