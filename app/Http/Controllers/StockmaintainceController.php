<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\SotckMaintainceRequest;
use App\Stockmaintaince;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockmaintainceController extends Controller
{
     function index(Request $request)
    {
        $query=Stockmaintaince::with('product','user');
        return $this->getListForUI($query, $request);
    }


    public function create()
    {
        //
    }


    public function store(SotckMaintainceRequest $request)
    {
        $employee=Employee::whereUserId(auth()->id())->first();
        $request->merge([
            'product_id'=>$request->input('product_id')['id'],
            'user_id'=>auth()->id(),
            'employee_id'=>$employee?$employee->id:0
        ]);
        Stockmaintaince::create($request->all());
        return response()->json([
            'type'=>'success',
            'message'=>'Stock crated successfully'
        ]);

    }


    public function show(Stockmaintaince $stockmaintaince)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        return Stockmaintaince::with('product')->whereId($id)->first();
    }


    public function update(Request $request, $id)
    {
        $request->merge(['product_id'=>$request->input('product')['id']]);
        $stock=Stockmaintaince::find($id);
        $stock->update($request->except('product'));
        return response()->json([
            'type'=>'success',
            'message'=>'Stock update successfully'
        ]);
    }


    public function destroy($id)
    {
        Stockmaintaince::find($id)->delete();
        return response()->json([
            'type'=>'success',
            'message'=>'Stock Maintaince deleted successfully'
        ]);
    }


}

