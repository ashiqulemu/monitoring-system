<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use App\Http\Requests\AttendanceRequest;
use App\PreRota;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AttendanceController extends Controller
{

    public function index(Request $request)
    {
        $difSearch=$request->input('differentSearch');
        $query=DB::table('attendances')
            ->leftJoin('employees','attendances.employee_id','=','employees.id')
            ->select("attendances.*" ,"employees.name","employees.id as employeeId",
                DB::raw(" GROUP_CONCAT(
      CONCAT('{\"clock_in\":\"', COALESCE(attendances.clock_in, ''), '\",\"details\":\"', COALESCE(details, ''), '\",\"id\":\"', COALESCE(attendances.id, ''), '\",\"early_time\":\"', COALESCE(early_time, ''), '\",\"type\":\"', COALESCE(type, ''), '\",\"late_time\":\"', COALESCE(late_time, ''), '\", \"clock_out\":\"', COALESCE(attendances.clock_out, ''),
             '\"}')) list"))
            ->groupBy(['employees.id',DB::raw("DATE_FORMAT(clock_in, '%Y-%m-%d')")]);
        if($difSearch){
            if($difSearch=='late'){
                $query->whereNotNull('attendances.late_time');
            }else{
                $query->where('attendances.type',$difSearch);
            }

        }
        return $this->getListForUI($query, $request);


    }


    public function create()
    {
        return Employee::whereIsActive(1)->get();
    }


    public function store(AttendanceRequest $request)
    {
        $request->merge([
            'clock_in'=>Carbon::parse($request->clock_in),
            'clock_out'=>Carbon::parse($request->clock_out),
            ]);

        
        Attendance::create([
            'employee_id'=>$request->input('employee')['id'],
            'clock_in'=>$request->input('clock_in'),
            'clock_out'=>$request->input('clock_out'),
            'details'=>$request->input('details'),
            'type'=>$request->input('type'),
        ]);
        return response()->json([
            'type'=>'success',
            'message'=>'Attendance create successfully'
        ]);
    }


    public function show(Attendance $attendance)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        $employees= Employee::whereIsActive(1)->get();
        $attendance=Attendance::with('employee')->whereId($id)->first();
        return response()->json([
            'employees'=>$employees,
            'attendance'=>$attendance
        ]);
    }


    public function update(AttendanceRequest $request, $id)
    {
        $request->merge([
            'clock_in'=>Carbon::parse($request->clock_in),
            'clock_out'=>Carbon::parse($request->clock_out),
        ]);

        $attendance=Attendance::find($id);
        $attendance->update([
            'employee_id'=>$request->input('employee')['id'],
            'clock_in'=>$request->input('clock_in'),
            'clock_out'=>$request->input('clock_out'),
            'details'=>$request->input('details'),
            'type'=>$request->input('type'),
        ]);
        return response()->json([
            'type'=>'success',
            'message'=>'Attendance updated successfully'
        ]);
    }


    public function destroy($id)
    {
        $attendance=Attendance::find($id);
        $attendance->delete();
        return response()->json([
            'type'=>'success',
            'message'=>'Attendance deleted successfully'
        ]);
    }


    public function getTodayAttendance(){
        return DB::table('attendances as a')
            ->leftJoin('employees as e','a.employee_id','=','e.id')
            ->select("a.*" , "e.name","e.id as employeeId",
                DB::raw(" GROUP_CONCAT(
      CONCAT('{\"clock_in\":\"', COALESCE(clock_in, ''), '\",\"details\":\"', COALESCE(details, ''), '\",\"early_time\":\"', COALESCE(early_time, ''), '\",\"late_time\":\"', COALESCE(late_time, ''), '\",\"type\":\"', COALESCE(type, ''), '\", \"clock_out\":\"', COALESCE(clock_out, ''),
             '\"}')) list"))
            ->whereDate('a.clock_in',Carbon::now()->format('Y-m-d'))
            ->groupBy('e.id')
            ->get();

//        return Attendance::with('employee')->whereDate('clock_in',Carbon::now()->format('Y-m-d'))->get();

    }




    public function staffLogout(Request $request)
    {
        $employee = Employee::whereUsername($request->username)->whereIsActive(1)->first();
        if($employee){
            if($employee->username == $request->username){
                if(Hash::check($request->password, $employee->password)){
                    $attendace = Attendance::whereEmployeeId($employee->id)
                        ->whereDate('clock_in',Carbon::now()->format('Y-m-d'))
                        ->where('clock_out',null)
                        ->orderBy('id','DESC')->first();
                    if($attendace){
                        $attendace->clock_out=Carbon::now()->toDateTimeString();
                        $attendace->details=$request->input('activities');
                        $attendace->save();
                        return response()->json([
                            'type'=>'success',
                            'message'=>'Logout successfully.'
                        ]);
                    }else{
                        return response()->json([
                            'type'=>'error',
                            'message'=>'You are not logged in. Please Login First'
                        ]);
                    }

                }else{
                    return response()->json([
                        'type'=>'error',
                        'message'=>'Your username and password not correct'
                    ]);
                }
            }else{
                return response()->json([
                    'type'=>'error',
                    'message'=>'Your username and password not correct'
                ]);
            }
        }else{
            return response()->json([
                'type'=>'error',
                'message'=>'Your username and password not correct'
            ]);
        }



    }



    public function staffLogin(Request $request)
    {
        $employee = Employee::whereUsername($request->username)->whereIsActive(1)->first();
        if($employee){
            if(Hash::check($request->password, $employee->password)){
                $hasAttandance=Attendance::whereEmployeeId($employee->id)
                    ->whereDate('clock_in',Carbon::now()->format('Y-m-d'))
                    ->where('clock_out',null)->orderBy('id','DESC')->first();
                if(!$hasAttandance){

                    $preRota = $this->checkHasPrerota($employee);



                       $earlyTime=null;
                        $lateTime=null;
                        if($preRota){
                            $time= Carbon::parse($preRota->start_time)->diff(Carbon::now())->format('%H:%I:%S');
                            $atendanceCount=Attendance::whereEmployeeId($employee->id)
                                ->whereDate('clock_in',Carbon::now()->format('Y-m-d'))
                                ->count();

                            if($atendanceCount<1){
                                if (Carbon::parse($preRota->start_time)<Carbon::now()) {
                                    $lateTime=$time;
                                }else{
                                    $earlyTime=$time;
                                }
                            }
                        }


                        Attendance::create([
                            'employee_id'=>$employee->id,
                            'clock_in'=>Carbon::now()->toDateTimeString(),
                            'early_time'=>$earlyTime,
                            'late_time'=>$lateTime,
                        ]);
                        return response()->json([
                            'type'=>'success',
                            'message'=>'Login Successfully your Clock in time start from now'
                        ]);

                }else{
                    return response()->json([
                        'type'=>'error',
                        'message'=>'You Already Logged in'
                    ]);
                }
            }else{
                return response()->json([
                    'type'=>'error',
                    'message'=>'Your username and password not correct'
                ]);
            }
        }else{
            return response()->json([
               'type'=>'error',
               'message'=>'Your username and password not correct'
            ]);
        }
    }

    /**
     * @param $employee
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection|static|static[]
     */
    public function checkHasPrerota($employee)
    {
        $preRota = PreRota::with('employees')
            ->whereDate('date', Carbon::now()->format('Y-m-d'));
        $preRota->whereHas('employees', function ($preRota) use ($employee) {
            $preRota->where('id', $employee->id);
        });
        $preRota = $preRota->first();
        return $preRota;
    }
}
