<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use App\Holiday;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{

    public function getDashboardData(){
        $startDate = date('Y') . '-04-01';

        $monthlyHoliday=Holiday::whereYear('date', Carbon::now()->year)
                ->whereMonth('date', Carbon::now()->month)->count();

        $todaySick=Attendance::whereDate('clock_in', Carbon::today())->whereType('Sick')->count();
        $todayAbsence=Attendance::whereDate('clock_in', Carbon::today())->whereType('Absence')->count();

        return response()->json([
           'monthlyHoliday'=>$monthlyHoliday,
           'todaySick'=>$todaySick,
           'todayAbsence'=>$todayAbsence,
        ]);

    }
    public function generatePayslip(Request $request){
        $formDate=$request->input('from_date').' 00:00:00';
        $toDate=$request->input('to_date').' 23:59:59';
        $employee=Employee::find($request->employee_id);
        $payslip=Attendance::whereType('Regular')
            ->whereEmployeeId($request->employee_id)
            ->whereBetween('clock_in',[$formDate,$toDate])
            ->get();

        $holidayController= new HolidayController();
        return response()->json([
            'payslip'=>$payslip,
            'employee'=>$employee,
            'holiday' =>$holidayController->getHolidayReport($request)

        ]);

    }

    public function getStockReport(Request $request){

        $query= DB::table('products as p')
            ->leftJoin('stockmaintainces as sm','sm.product_id','=','p.id')
            ->select('p.name','p.id',
                DB::raw("sum(CASE when sm.cloth_type='Initial Stock' THEN sm.quantity 
                ELSE NULL END) as initial_stock,
            sum(CASE when sm.cloth_type='Dirty' THEN sm.quantity ELSE NULL END) as dirty,
            sum(CASE when sm.cloth_type='Bed' THEN sm.quantity ELSE NULL END) as bed,
            sum(CASE when sm.cloth_type='Arrived' THEN sm.quantity ELSE NULL END) as arrived,
            sum(CASE when sm.cloth_type='Lost/Missed' THEN sm.quantity ELSE NULL END) as lost,
             sum(sm.marks) as marks
            "))
            ->groupBy('p.id');

        return $this->getListForUI($query, $request);
    }

    public function getStockReportByStaff(Request $request){

        $query= DB::table('users as u')
            ->leftJoin('stockmaintainces as sm','sm.user_id','=','u.id')
            ->leftJoin('products as p','p.id','=','sm.product_id')
            ->select('u.id','u.name','p.name as product_name','p.id as product_id',
                DB::raw("sum(CASE when sm.cloth_type='Initial Stock' THEN sm.quantity 
                ELSE NULL END) as initial_stock,
            sum(CASE when sm.cloth_type='Dirty' THEN sm.quantity ELSE NULL END) as dirty,
            sum(CASE when sm.cloth_type='Bed' THEN sm.quantity ELSE NULL END) as bed,
            sum(CASE when sm.cloth_type='Arrived' THEN sm.quantity ELSE NULL END) as arrived,
            sum(CASE when sm.cloth_type='Lost/Missed' THEN sm.quantity ELSE NULL END) as lost,
             sum(sm.marks) as marks
            "))
            ->groupBy('u.id');

        return $this->getListForUI($query, $request);
    }


    public function getEmployeeReport(Request $request){
        $formDate=$request->from_date.' 00:00:00';
        $toDate=$request->to_date.' 23:59:59';
        $query=DB::table('attendances')
            ->leftJoin('employees','attendances.employee_id','=','employees.id')
            ->select("attendances.*" ,"employees.name","employees.id as employeeId",
                DB::raw(" GROUP_CONCAT(
      CONCAT('{\"clock_in\":\"', COALESCE(attendances.clock_in, ''), '\",\"details\":\"', COALESCE(details, ''), '\",\"id\":\"', COALESCE(attendances.id, ''), '\",\"early_time\":\"', COALESCE(early_time, ''), '\",\"late_time\":\"', COALESCE(late_time, ''), '\", \"clock_out\":\"', COALESCE(attendances.clock_out, ''),
             '\"}')) list"))
            ->whereBetween('attendances.clock_in',[Carbon::parse($formDate),Carbon::parse($toDate)])
            ->whereNotNull('attendances.created_at')
            ->where('employees.id',$request->input('employee'))
            ->groupBy(DB::raw("DATE_FORMAT(clock_in, '%Y-%m-%d')"));

        return $this->getListForUI($query, $request);
    }




}
