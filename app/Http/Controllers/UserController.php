<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request){
        $query=User::where('role','!=','staff')->select();
        return $this->getListForUI($query, $request);
    }

    public function create()
    {

    }


    public function store(UserRequest $request)
    {
        $request->merge(['password'=>bcrypt($request->input('password'))]);
        User::create($request->all());

        return response()->json([
            'type'=>'success',
            'message'=>'User has been created successfully'
        ]);

    }

    public function show(User $employee)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        return User::find($id);
    }

    public function update(UserRequest $request, $id)
    {
        $loggedInUser=User::find(auth()->user()->id);

        if($loggedInUser->role=='admin'){
                if($request->input('password')){
                    $request->merge(['password'=>bcrypt($request->input('password'))]);
                }
                $user=User::find($id);
                $user->update($request->all());
                return $this->feedBackMessage('success','User has been updated successfully');


        }else{
            return $this->feedBackMessage('error','You are not admin you know');
        }

    }

    public function feedBackMessage($type,$message){
        return response()->json([
            'type'=>$type,
            'message'=>$message
        ]);
    }

    public function destroy($id)
    {
        $user=User::find($id);
        if($user->role!='admin'){
            $user->delete();
            return response()->json([
                'type'=>'success',
                'message'=>'User deleted successfully'
            ]);
        }

    }



}
