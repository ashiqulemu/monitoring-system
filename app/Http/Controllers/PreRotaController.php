<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Holiday;
use App\Http\Requests\PrerotaRequest;
use App\PreRota;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PreRotaController extends Controller
{

    public function index(Request $request)
    {
        $query=PreRota::with('employees');
        return $this->getListForUI($query, $request);
    }


    public function create()
    {
        return response()->json([
            'employees'=>Employee::whereIsActive(1)->orderBy('name')->get(),
            'holiday'=>Holiday::with('employee')->whereDate('date',Carbon::today())->get()
        ]);
    }


    public function store(PrerotaRequest $request)
    {

            $preRota = PreRota::create(collect($request->all())->except(['employee'])->toArray());

            if(array_key_exists('employee',$request->all())){
                $employees=$request['employee'];
                $ids=[];
                foreach ($employees as $item){
                    $holiday = Holiday::whereEmployeeId($item['id'])
                        ->whereDate('date',Carbon::today())->count();

                    if(!$holiday){
                        $ids[]=$item['id'];
                    }

                }
                $preRota->employees()->attach($ids);
            }


        return response()->json([
            'type'=>'success',
            'message'=>'Pre Rota Created successfully'
        ]);

    }


    public function show(PreRota $preRota)
    {
        //
    }


    public function edit(PreRota $preRota)
    {
        //
    }


    public function update(PrerotaRequest $request, $id)
    {
        $preRota=PreRota::find($id);
        $preRota->update($request->except(['employee','employees']));

        if(array_key_exists('employee',$request->all())){
            $employees=$request['employee'];
            $ids=[];
            foreach ($employees as $item){
                $holiday = Holiday::whereEmployeeId($item['id'])
                    ->whereDate('date',Carbon::today())->count();
                if(!$holiday){
                    $ids[]=$item['id'];
                }
            }
            $preRota->employees()->sync($ids);
        }


        return response()->json([
            'type'=>'success',
            'message'=>'Pre Rota Created successfully'
        ]);
    }


    public function destroy(Request $request,$id)
    {
        $employee=PreRota::find($id);
        $employee->delete();
        return response()->json([
            'type'=>'success',
            'message'=>'Pre Rota Deleted successfully'
        ]);
    }

    public function getData(Request $request){

        if($request->input('dateInput')){
            $date=Carbon::parse($request->input('dateInput'))->addDays(6);
            $fromDate=Carbon::parse($request->input('dateInput'));
        }else{
            $date = Carbon::today()->addDays(6);
            $fromDate=Carbon::today();
        }

        $preRota=PreRota::with('employees')
            ->whereBetween('date',[$fromDate,$date])
            ->orderBy('date');
        if($request->input('employeeList')){
            $preRota->whereHas('employees',function ($preRota) use ($request){
               $preRota->where('id',$request->input('employeeList'));
            });

        }
        $preRota=$preRota->get();

        return response()->json([
            'employees'=>Employee::whereIsActive(1)->orderBy('name')->get(),
            'preRota'=>$preRota
        ]);
    }
}
