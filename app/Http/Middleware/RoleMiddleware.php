<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=Auth::user();
        $url=request()->path();
        if($user){
            if($user->role=='admin'){
                return $next($request);

            }elseif($user->role=='subadmin'){
                if(str_contains($url,'user')){
                    return redirect('/login');
                }else{
                    return $next($request);
                }

            }elseif($user->role=='staff'){

                if(str_contains($url,'stock')){
                    return $next($request);

                }else{
                    return redirect('/login');
                }

                if(str_contains($url,'product')){
                    return $next($request);

                }else{
                    return redirect('/login');
                }
            }else{
                return redirect('/login');
            }
        }else{
            return redirect('/login');
        }

    }
}
