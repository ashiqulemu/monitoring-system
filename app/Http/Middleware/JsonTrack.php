<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Response;

class JsonTrack
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->isXmlHttpRequest()) {
            return new Response(view('home'));
        }
        return $next($request);
    }
}
