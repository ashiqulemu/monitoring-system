<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=>'required|unique:employees,username,'.$this->employee,
            'name'=>'required',
            'employee_id'=>'required|unique:employees,employee_id,'.$this->employee,
            'pay_rate'=>'required',
            'tax_code'=>'required',
            'employee_type'=>'required',
            'password'=>'required',

        ];
    }
}
